/**
 * cplayer - Simple, GEMA-Free Youtube player
 * 
 * Written and copyrighted 2013
 * by Sven Marc 'CybroX' Gehring
 *
 * This project is licensed under CC BY-NC-SA.
 * For more informations please read the License-Deed
 * > http://creativecommons.org/licenses/by-nc-sa/3.0/
 *
 * Current Version:		0.1 - indev
 */

/* global player configuration */
var downloader 	= "http://convert2mp3.net/c-mp3.php?url=http://youtube.com/?v=";
var url 		= "http://music2.cybrox.eu/";
var defaultvid	= "r7ngosPaTls";

/* define hotkeys */
document.onkeydown=function(e){
	if(e.which == 16){toggleHelp();}
	if(e.which == 32){togglePlayState();}
	if(e.which == 37){getPreviousTrack();}
	if(e.which == 39){getNextTrack();}
	if(e.which == 38){changeVolume(1);}
	if(e.which == 40){changeVolume(-1);}
	if(e.which == 73){toggleImage();}
	if(e.which == 76){toggleLoop();}
	if(e.which == 83){toggleShuffle();}
	if(e.which == 68){window.open(downloader+tracklinks[track]);}
}

/* get settings from php framework using html-attributes */
var loop 	= (document.body.getAttribute("loop") == 0 ? false : true);
var shuf 	= (document.body.getAttribute("shuffle") == 0 ? false : true);
var imge 	= (document.body.getAttribute("image") == 0 ? false : true);
var trck	= (document.body.getAttribute("track") - 1);
var time	=  document.body.getAttribute("time");
var mode 	=  document.body.getAttribute("mode");
var main	=  document.body.getAttribute("state");
var plst	=  false;		// Display playlist indow
var help	=  false;		// Display help window
var play	=  true;		// Play current track
var track	=  0;			// Starttrack
var trcnt	=  0;			// Trackcount
var cuid	=  new Array;	// Current video's id
var cnme	=  new Array;	// Current video's name
var cvid	=  new Array;	// Current video's data
var tmpl	=  new Array;	// Temporary Playlist (used while "starting up")
var temp	=  "";			// Temporary Text variable

/* prechange options */
if(loop){loop=false; toggleLoop();}
if(shuf){shuf=false; toggleShuffle();}
if(imge){imge=false; toggleImage();}
document.getElementById("playlistbox").style.height = window.innerHeight+"px";

/* load different player modes */
switch(mode){
	case 'singleid':
		track = 0;
		trcnt = 1;
		cuid[0] = main;
		cnme[0] = "Youtube-Stream ["+main+"]";
		toggleLoop();		// singleid will always be looped
		break;
	case 'multipleids':
		track = trck;
		tmpl = main.split(',');
		trcnt = tmpl.length;
		for(var i in tmpl){
			cuid[i] = tmpl[i];
			cnme[i] = "Youtube-Stream ["+tmpl[i]+"]";
		}
		break;
}

/************ SYSTEMLOADER-FUNCTIONS ************/
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

/* start when youtube player is loaded */
function onPlayerReady(event) {
	setInterval("updateProgressbar();", 1000);
	if(time != 0){player.seekTo(time);}
	player.playVideo();
	updatePlayerFrontend();
}

/* player state-change eventhandler */
function onPlayerStateChange(event) {
	if(event.data == 0){
		/* get next track if loop is disabled */
		if(!loop){getNextTrack();} else {player.loadVideoById(cuid[track], 0, "large");}
	}
}
		
var player;
function onYouTubeIframeAPIReady() {
	player = new YT.Player('player', {
		height: '2',
		width: '2',
		videoId: cuid[track],
		events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange,
			'onError': onPlayerError
		}
	});
}

/* handle youtube-player errors */
function onPlayerError(error){
	player.pauseVideo();
	document.getElementById("sm_left").innerHTML = "";
	document.getElementById("sm_right").innerHTML = "<br />(╯°□°）╯︵ ┻━┻";
	switch(error){
		case 2: document.getElementById("title").innerHTML = "Request failed - Unknown Tracklink"; break;
		case 100: document.getElementById("title").innerHTML = "Request failed - Track not Found / Removed by owner"; break;
		case 101: document.getElementById("title").innerHTML = "Request failed - Track embed disabled by it's owner"; break;
		case 150: document.getElementById("title").innerHTML = "Request failed - Track embed disabled by it's owner"; break;
		default: document.getElementById("title").innerHTML = "Request failed - Unknown error / Invalid Youtube ID"; break;
	}
	if(mode != 'singleid'){window.setTimeout('getNextTrack()', 2000);}
}

/* update the players frontend */
function updatePlayerFrontend(){
	document.getElementById("bt_dld").href = downloader+cuid[track];
	document.getElementById("title").innerHTML = cnme[track];
	var totalDuration = parseInt(Math.floor(player.getDuration()));
	document.getElementById("ttime").innerHTML = totalDuration; //.toTime();
	
	loadVideoData();
}

/* update progress bar every second */
function updateProgressbar(){
	var newBar = (player.getCurrentTime() / player.getDuration()) * 100;
	document.getElementById("progressBarInner").style.width = newBar+"%";
	var currentTime = parseInt(Math.floor(player.getCurrentTime()));
	document.getElementById("ctime").innerHTML = currentTime; //.toTime();
}

/************ PLAYER-INTERFACE-FUNCTIONS ************/
/* load the previous track in playlist */
function getPreviousTrack(){
	if(!shuf){
		track = (track == 0) ? (trcnt-1) : track - 1;
	} else {
		track = Math.round((Math.random() * ((trcnt-1) - 0)) + 0);
	}
	updatePlayerFrontend();
	player.loadVideoById(cuid[track], 0, "large");
}

/* load the next track in playlist */
function getNextTrack(){
	if(!shuf){
		track = (track == (trcnt-1)) ? 0 : track + 1;
	} else {
		track = Math.round((Math.random() * ((trcnt-1) - 0)) + 0);
	}
	updatePlayerFrontend();
	player.loadVideoById(cuid[track], 0, "large");
}

/* play / pause the current track */
function togglePlayState(){
	if(play){
		play = false; player.pauseVideo();
		document.getElementById("bt_pas").style.display = "none";
		document.getElementById("bt_ply").style.display = "inline-block";
	} else {
		play = true; player.playVideo();
		document.getElementById("bt_pas").style.display = "inline-block";
		document.getElementById("bt_ply").style.display = "none";
	}
}

/* toggle the help-box */
function toggleHelp(){
	if(help == 0){document.getElementById("help").style.display = "block"; help = 1;
	} else {document.getElementById("help").style.display = "none"; help = 0;}
}

/* toggle the loop-funtion */
function toggleLoop(){
	if(loop){loop = false; document.getElementById("bt_lop").style.color = "#404040";}
	else {loop = true; document.getElementById("bt_lop").style.color = "#ececec";}
}

/* toggle the shuffle-funtion */
function toggleShuffle(){
	if(shuf){shuf = false; document.getElementById("bt_shf").style.color = "#404040";}
	else {shuf = true; document.getElementById("bt_shf").style.color = "#ececec";}
}

/* toggle the image-funtion */
function toggleImage(){
	if(imge){
		imge = false;
//		document.getElementById("thumbnail_bg").style.display = "none";
		document.getElementById("bt_img").style.color = "#404040";
	} else {
		imge = true;
//		document.getElementById("thumbnail_bg").style.display = "block";
		document.getElementById("bt_img").style.color = "#ececec";
	}
}

/* change volume per click-on-bar */
jQuery(document).ready(function(){
	$("#volume").click(function(e){
		player.setVolume(e.pageX - this.offsetLeft);
		updateVolumeDisplay(e.pageX - this.offsetLeft);
	}); 
})

/* change volume with a given +/- parameter */
function changeVolume(change){
	var newVolume = player.getVolume()+change;
	player.setVolume(newVolume);
	updateVolumeDisplay(newVolume);
}

/* update the volume-bar */
function updateVolumeDisplay(newVolume){
	document.getElementById("volumeInner").style.width = newVolume+"px"; /*<- div:100px*/
}

/************ PLAYER-WARNINGS-AND-NOTICES ************/
function dlw(toggle){document.getElementById("dl_wrn").style.bottom = (toggle == 1) ? "60px" : "-60px";}


/************ PLAYLIST WINDOW ************/
function togglePlaylistWindow(){
	alert('a');
	if(plst){
		with (document.getELementById("playlistbox").style) {
			display = "none";
			width = "0%";
		}
		document.getELementById("cplayerbox").style.width = "100%";
	} else {
		with (document.getELementById("playlistbox").style) {
			display = "block";
			width = "40%";
		}
		document.getELementById("cplayerbox").style.width = "60%";
	}
	plst = !plst;
}


/************ YOUTUBE DATA API ************/
function youtubeDataCallback(data) {
	cvid['title'] = data.entry.title.$t;
	cvid['ratin'] = data.entry.gd$rating.average.toFixed(1);
	cvid['views'] = data.entry.yt$statistics.viewCount;
	
	if(mode == 'singleid' || mode == 'multipleids'){
		document.getElementById("title").innerHTML = cvid['title'];
	}
	
	document.getElementById("viewd").innerHTML = cvid['views'];
	
	for (var i=0;i<(Math.round(cvid['ratin']));i++){temp += '<i class="icon-star"></i>';}
	document.getElementById("good").innerHTML = temp; temp = "";
	for (var i=0;i<(Math.round((5 - cvid['ratin'])));i++){temp += '<i class="icon-star"></i>';}
	document.getElementById("baad").innerHTML = temp;
}
	
function loadVideoData(){
	$(document).ready(function() {
		$.getScript('http://gdata.youtube.com/feeds/api/videos/'+encodeURIComponent(cuid[track])+'?v=2&alt=json-in-script&callback=youtubeDataCallback');
	});
}

String.prototype.toTime = function () {
    sec_numb    = parseInt(this);
    var hours   = Math.floor(sec_numb / 3600);
    var minutes = Math.floor((sec_numb - (hours * 3600)) / 60);
    var seconds = sec_numb - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = hours+':'+minutes+':'+seconds;
    return time;
}