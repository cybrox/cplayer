<?php

/**
 * cplayer - Simple, GEMA-Free Youtube player
 * 
 * Written and copyrighted 2013
 * by Sven Marc 'CybroX' Gehring
 *
 * This project is licensed under CC BY-NC-SA.
 * For more informations please read the License-Deed
 * > http://creativecommons.org/licenses/by-nc-sa/3.0/
 *
 * Current Version:		0.1 - indev
 */
 

/* initialize a new player session * /
session_start();
mysql_connect("localhost", "***********", ""***********",");
mysql_select_db(""***********","); */
define("PLAYER_URL", "http://music2.cybrox.eu/");

/* read the current url */
$url = explode("/", $_SERVER["REQUEST_URI"]);

/* read all parameters */
$p_main = (!empty($url[1])) ? $url[1] : "";
	/* Possible values:
	 * - Youtube ID, 11 Chars, no delimiter
	 * - Multiple Youtube ID's, delimiter = ","
	 * - Playlist Key, X Chars, no delimiter
	 * - Playlist Key + track, X Chats, delimiter : ","
	 */
$p_parm = (!empty($url[2])) ? $url[2] : "";
	/* Possible properties: (delimiter : ",")
	 * - loop  [BOOL] - Toggle loop function
	 * - image [BOOL] - Toggle background-image
	 * - track  [INT]  - Jump to the # track in playlist
	 * - time  [INT]  - Jump to the given time in seconds
	 */

/* return error if the player has nothing to play */
if(strlen($p_main) < 10){$critical = true;}
	 
/* progress $p_main to get the right player-mode */
$p_mode = "undefined";
if(!strstr($p_main, "#")){if(!strstr($p_main, ",")){$p_mode = "singleid";} else {$p_mode = "multipleids";}}
else{if(!strstr($p_main, ",")){$p_mode = "playlist";} else {$p_mode = "trackinlist";}}

/* Progress $p_parm to get the player parameters */
$p_loop = 0;	// Deactivate the loop function as default
$p_shuf = 0;	// Deactivate the shuffle function as default
$p_imge = 0;	// Deactivate the image function as default
$p_track = 1;	// Set the default start-track to 1
$p_time = 0;	// Set the default start-time to 0 seconds

/* Get all the optional parameters (Syntax : /loop=1,shuffle=1,image=1,time=20/) */
if(!empty($p_parm)){
	if(strstr($p_parm, "loop"))    if(preg_match("#loop=(.*?)(,|$)#", $p_parm, $aa)) $p_loop = $aa[1][0];
	if(strstr($p_parm, "shuffle")) if(preg_match("#shuffle=(.*?)(,|$)#", $p_parm, $ab)) $p_shuf = $ab[1][0];
	if(strstr($p_parm, "image"))   if(preg_match("#image=(.*?)(,|$)#", $p_parm, $ac)) $p_imge = $ac[1][0];
	if(strstr($p_parm, "track"))   if(preg_match("#track=(.*?)(,|$)#", $p_parm, $ad)) $p_track = $ad[1];
	if(strstr($p_parm, "time"))    if(preg_match("#time=(.*?)(,|$)#", $p_parm, $ae)) $p_time = $ae[1];
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
	<link rel="shortcut icon" href="<?php echo PLAYER_URL; ?>style/img/favicon.ico" type="image/x-icon" />
	<link rel="icon" href="<?php echo PLAYER_URL; ?>style/img/favicon.ico" type="image/x-icon" />
	
	
	<link rel="stylesheet" type="text/css" href="<?php echo PLAYER_URL; ?>style/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo PLAYER_URL; ?>style/font/font-awesome.css" />
	
	
	<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
	
	<title>d(^_^)b CybMusic</title>
</head>
<body id="cplayer" <?php echo 'mode="'.$p_mode.'" state="'.$p_main.'" loop="'.$p_loop.'" shuffle="'.$p_shuf.'" image="'.$p_imge.'" track="'.$p_track.'" time="'.$p_time.'"'; ?>>
<div id="cplayerbox">	
    <div id="player"></div>
	
	<div id="track">
		<span id="sm_left">(&gt;^_^)&gt; </span>
		<span id="title">Loading Tracks from Playlist</span>
		<span id="sm_right"> &lt;(^_^&lt;)</span>
		<br />
		<span id="tit_pl"><noscript>Error: You need to activate JavaScript to use CybPlayer!</noscript></span>
	</div>
	
	<div id="interface">
		<div id="progressBarContainer">
			<div id="progressBar">
				<div id="progressBarInner"></div>
			</div>
		</div>
		<script type="text/javascript">
			/* calculate total video duration in "px on screen" */
			var totalDur = document.getElementById("progressBar").offsetWidth;
			/* seek to the chosen time on clicking the progress bar*/
			jQuery(document).ready(function(){
				$("#progressBar").click(function(e){
					var durPercentage = (e.pageX - this.offsetLeft) / totalDur;
					player.seekTo((player.getDuration() * durPercentage));
				}); 
			})
		</script>
		<div id="controllerContainer">
			<div id="controllerLeft">
				<a id="bt_hlp" onclick="toggleHelp()"><i class="icon-question-sign"></i></a>
				<a id="bt_dld" href="#" target="_blank" onmouseover="dlw(1)" onmouseout="dlw(0)"><i class="icon-download-alt"></i></a>
				<a id="bt_shf" onclick="toggleShuffle()"><i class="icon-random"></i></a>
				<a id="bt_lop" onclick="toggleLoop()"><i class="icon-repeat"></i></a>
				<a id="bt_img" onclick="toggleImage()"><i class="icon-picture"></i></a>
			</div>
			<div id="controllerMiddle">
				<a id="bt_prv" onclick="getPreviousTrack()"><i class="icon-step-backward"></i></a>
				<a id="bt_ply" onclick="togglePlayState()"><i class="icon-play"></i></a>
				<a id="bt_pas" onclick="togglePlayState()"><i class="icon-pause"></i></a>
				<a id="bt_nxt" onclick="getNextTrack()"><i class="icon-step-forward"></i></a>
			</div>
			<div id="controllerRight">
				<a id="bt_vdn" onclick="changeVolume(-1)"><i class="icon-volume-down"></i></a>
				<div id="volume"><div id="volumeInner"></div></div>
				<a id="bt_vup" onclick="changeVolume(1)"><i class="icon-volume-up"></i></a>
			</div>
			<div style="clear:left"></div>
		</div>
	</div>
	<p id="dl_wrn">
		<span style="color:red">Attention!</span><br/>
		You are not allowed to share or (re)upload this mp3 file!
		This is <u>illegal</u> in <u>any</u> way! - So just enjoy for
		yourself ;)
	</p>
	<div id="help"><div id="helpInner">
		<h2>d(^_^)b CybPlayer Help</h2>
		<h3>Shortcuts</h3>
		<hr />
		<table width="100%">
			<tr><td><i class="icon-volume-up"></i></td><td class="l">Vol. Up</td><td><i class="icon-caret-up"></i></td><td>+</td></tr>
			<tr><td><i class="icon-volume-down"></i></td><td class="l">Vol. Down</td><td><i class="icon-caret-down"></i></td><td>-</td></tr>
			<tr><td><i class="icon-step-forward"></i></td><td class="l">Next Track</td><td><i class="icon-caret-right"></i></td><td></td></tr>
			<tr><td><i class="icon-step-backward"></i></td><td class="l">Previous Track</td><td><i class="icon-caret-left"></i></td><td></td></tr>
			<tr><td><i class="icon-pause"></i></td><td class="l">Pause / Play</td><td>SPACE</td><td></td></tr>
			<tr><td><i class="icon-random"></i></td><td class="l">Toggle Shuffle</td><td>S</td><td></td></tr>
			<tr><td><i class="icon-repeat"></i></td><td class="l">Toggle Loop</td><td>L</td><td></td></tr>
			<tr><td><i class="icon-picture"></i></td><td class="l">Toggle Thumbnail-Background</td><td>I</td><td></td></tr>
			<tr><td><i class="icon-download-alt"></i></td><td class="l">Download</td><td>D</td><td></td></tr>
			<tr><td><i class="icon-question-sign"></i></td><td class="l">Help</td><td>SHIFT</td><td></td></tr>
		</table>
		<hr />
		<div align="right"><br />Press SHIFT to close.</div>
	</div></div>
	
	<!--
	<span id="ctime">00:00</span><span id="ttime">00:00</span><span id="viewd">0</span>
	<div id="rating">
		<span id="good"></span>
		<span id="baad"><i class="icon-star-empty"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i></span>
	</div>
	-->
</div>
<div id="playlistbox">
	<div id="playlistboxInner">
		some text here
	</div>
</div>
	
<!-- start cplayer -->
<?php
	if(!$critical){
		echo '<script src="'.PLAYER_URL.'cplayer.js" type="text/javascript"></script>';
	} else {
		echo '<script type="text/javascript">document.getElementById("title").innerHTML = "Error, invalid Youtube-ID";</script>';
#		header("Location: ".PLAYER_URL."home.php");	
	}
?>
	
<body>
</html>